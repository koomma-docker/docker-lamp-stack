#!/bin/bash

#create dir for mysql data
mkdir -p ../data/mysql
mkdir -p ../db
mkdir -p ../logs/apache2
mkdir -p ../logs/mysql

if [ -d ../../app/cache ]
then
    rm -rf ../../app/cache
fi

if [ -d ../../app/logs ]
then
    rm -rf ../../app/logs
fi

exit 0
