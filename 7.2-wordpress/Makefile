include .env
DOCK=docker-compose -p $(APP_NAME)
RUN=$(DOCK) run --rm
EXEC=$(DOCK) exec --user deployer webserver
EXEC_WP=$(DOCK) exec -w /var/www/html/wordpress --user deployer webserver
EXEC_ROOT=$(DOCK) exec webserver
EXEC_MYSQL=$(DOCK) exec mysql
.PHONY: help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

stop: kill remove ## Global - Remove

# ----------------------------
# Internals
build: ## Docker - Build Image
	$(DOCK) build  --build-arg DOCKER_USER_UID=$(DOCKER_USER_UID) --build-arg DOCKER_USER_GID=$(DOCKER_USER_GID)

up: ## Docker - Up
	$(DOCK) up -d
	$(EXEC_ROOT) usermod -u $(DOCKER_USER_UID) deployer
	$(EXEC_ROOT) groupmod -g $(DOCKER_USER_GID) www-data

kill: ## Docker - Kill
	$(DOCK) kill

remove: ## Docker - Remove
	$(DOCK) rm -vf

shell: ## Display Shell
	$(EXEC) /bin/bash

shell-root: ## Display Shell
	$(EXEC_ROOT) /bin/bash

db-init: db-wait db-create db-auth## Init DB

db-init-import: db-wait db-create db-auth db-import## Init DB from file

db-wait: ## Symfony - Doctrine Wait for database connection
	$(EXEC_MYSQL) sh /root/mysql/check-mysql-service.sh $(MYSQL_DB_ROOT_PASSWORD)

db-create: ## Base Create DB
	$(EXEC_MYSQL) mysql -u root -p$(MYSQL_DB_ROOT_PASSWORD) -e "create database $(MYSQL_DB_NAME);"

db-auth: ## Auth Create
	$(EXEC_MYSQL) mysql -u root -p$(MYSQL_DB_ROOT_PASSWORD) -e "GRANT ALL PRIVILEGES ON $(MYSQL_DB_NAME).* TO $(MYSQL_DB_USER)@'%' IDENTIFIED BY '$(MYSQL_DB_PASSWORD)';"

db-import:
	$(EXEC_MYSQL) sh /root/mysql/import-database.sh $(MYSQL_DB_ROOT_PASSWORD) $(MYSQL_DB_NAME) /tmp/$(MYSQL_DATABASE_FILE)

db-shell: ## Display DB Shell
	$(EXEC_MYSQL) /bin/bash

wp-composer-create-project: ## Create Composer Project
	$(EXEC) composer create-project leoo-wp-bundle/wordpress-install wordpress --repository-url=https://satis.leoo-factory.io

wp-new-install: wp-composer-create-project wp-install-composer ## Fresh New Install
	$(EXEC) sh wordpress/config/install/wordpress-new-install.sh
	$(EXEC) sh wordpress/config/install/wordpress-new-install-extensions.sh

wp-install-composer: ## Wordpress Composer Install
	$(EXEC_WP) composer install

wp-update-composer: ## Wordpress Composer Update
	$(EXEC_WP) composer update

wp-perm: ## Wordpress Permissions
	$(EXEC_WP) chmod -R 777 web/wp-media
	$(EXEC_WP) chmod -R 777 web/wp-sites/themes/wordpress-koomma-$(APP_NAME)/acf-json

wp-install: wp-install-composer
	$(EXEC) sh wordpress/config/install/wordpress-install.sh
	$(EXEC) sh wordpress/config/install/wordpress-install-extensions.sh

wp-update: wp-update-composer
	$(EXEC) sh wordpress/config/install/wordpress-install.sh
	$(EXEC) sh wordpress/config/install/wordpress-install-extensions.sh