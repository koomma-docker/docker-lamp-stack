include .env
LATEST_RELEASE := $(shell ls -t ../.. | head -1)
DOCK_WEBSERVER_ID := $(shell docker ps | grep "$(APP_NAME)-prod-webserver-" | awk '{print $$1;}')
DOCK_MYSQL_ID := $(shell docker ps | grep "$(APP_NAME)-prod-mysql-" | awk '{print $$1;}')
DOCK_REDIS_ID := $(shell docker ps | grep "$(APP_NAME)-prod-redis-" | awk '{print $$1;}')
DOCK=docker
EXEC=$(DOCK) exec -u deployer
EXEC_WP=$(DOCK) exec -w /var/www/html/releases/$(LATEST_RELEASE)/wordpress -u deployer
EXEC_LATEST=$(DOCK) exec -u deployer -w /var/www/html/releases/$(LATEST_RELEASE)
EXEC_ROOT=$(DOCK) exec
EXEC_MYSQL=$(DOCK) exec
.PHONY: help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

stop: kill remove ## Global - Remove

debug-webserver: ## Debug Process
	@echo $(DOCK_WEBSERVER_ID)

debug-mysql: ## Debug MYSQL Process
	@echo $(DOCK_WEBSERVER_ID)

shell: ## Display Shell
	$(EXEC) -it $(DOCK_WEBSERVER_ID) bash

shell-root: ## Display Shell
	$(EXEC_ROOT) -it $(DOCK_WEBSERVER_ID) bash

db-init: db-wait db-create db-auth## Init DB

db-init-import: db-wait db-create db-auth db-import## Init DB from file


db-wait: ## Symfony - Doctrine Wait for database connection
	$(EXEC_MYSQL) $(DOCK_MYSQL_ID) sh /root/mysql/check-mysql-service.sh $(MYSQL_DB_ROOT_PASSWORD)

db-create: ## Base Create DB
	$(EXEC_MYSQL) $(DOCK_MYSQL_ID) mysql -u root -p$(MYSQL_DB_ROOT_PASSWORD) -e "create database $(MYSQL_DB_NAME);"

db-auth: ## Auth Create
	$(EXEC_MYSQL) $(DOCK_MYSQL_ID) mysql -u root -p$(MYSQL_DB_ROOT_PASSWORD) -e "GRANT ALL PRIVILEGES ON $(MYSQL_DB_NAME).* TO $(MYSQL_DB_USER)@'%' IDENTIFIED BY '$(MYSQL_DB_PASSWORD)';"

db-import:
	$(EXEC_MYSQL) $(DOCK_MYSQL_ID) sh /root/mysql/import-database.sh $(MYSQL_DB_ROOT_PASSWORD) $(MYSQL_DB_NAME) /tmp/$(MYSQL_DATABASE_FILE)

db-shell: ## Display DB Shell
	$(EXEC_MYSQL) -it $(DOCK_MYSQL_ID) bash

wp-new-install: wp-install-composer ## Fresh New Install
	$(EXEC_LATEST) $(DOCK_WEBSERVER_ID) sh wordpress/config/install/wordpress-new-install.sh
	$(EXEC_LATEST) $(DOCK_WEBSERVER_ID) sh wordpress/config/install/wordpress-new-install-extensions.sh

wp-install-composer: ## Wordpress Composer Install
	$(EXEC_WP) $(DOCK_WEBSERVER_ID) composer install

wp-update-composer: ## Wordpress Composer Update
	$(EXEC_WP) $(DOCK_WEBSERVER_ID) composer install

wp-perm: ## Wordpress Permissions
	$(EXEC_WP) $(DOCK_WEBSERVER_ID) chmod -R 777 web/wp-media
	$(EXEC_WP) $(DOCK_WEBSERVER_ID) chmod -R 777 web/wp-sites/themes/wordpress-koomma-$(APP_NAME)/acf-json

wp-install: wp-install-composer
	$(EXEC_LATEST) $(DOCK_WEBSERVER_ID) sh wordpress/config/install/wordpress-install.sh
	$(EXEC_LATEST) $(DOCK_WEBSERVER_ID) sh wordpress/config/install/wordpress-install-extensions.sh

wp-update: wp-update-composer
	$(EXEC_LATEST) $(DOCK_WEBSERVER_ID) sh wordpress/config/install/wordpress-install.sh
	$(EXEC_LATEST) $(DOCK_WEBSERVER_ID) sh wordpress/config/install/wordpress-install-extensions.sh

webserver-stop: ## Stop WebServer
	$(DOCK) stop $(DOCK_WEBSERVER_ID)

fix-symlink: ## Fix Symlink
	$(EXEC_WP) $(DOCK_WEBSERVER_ID) ln -s /var/www/html/shared/wp-media web/wp-media